1.Pull Docker image -
docker pull subidita/flask_view_csv_api_img:latest

2.Run Docker image -
docker run -dit --name my_flask_view_csv_api_container -p 8080:8080 subidita/flask_view_csv_api_img:latest

3.Test the api on postman with this collection link -
https://www.getpostman.com/collections/acf3bea4208bcee5344f
(Since the SSL certificate is self-signed please turn off SSL certificate verification under settings in postman)
