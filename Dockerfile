FROM python:3
WORKDIR ~/
#Copy from the local current dir to the image workdir:
COPY . .
#Install any dependencies listed in ./requirements.txt:
RUN pip install --no-cache-dir -r requirements.txt
#Run view_csv.py on container startup:
CMD [ "python", "./view_csv.py" ]