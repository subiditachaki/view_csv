from flask import Flask, jsonify, Response
from pandas import DataFrame, read_csv

app = Flask(__name__)

@app.route('/<int:value>')
def key_value_pair_from_csv(value):
    input_from_csv_dataframe = read_csv("Corpus.csv", index_col = False)
    row_from_csv = input_from_csv_dataframe.loc[input_from_csv_dataframe['value'] == value].to_dict('records')
    try:
        response = row_from_csv[0]
    except IndexError:
        return jsonify(error="Oops! this page does not exist"), 404
    return jsonify(response), 200

@app.errorhandler(404)
def page_not_found(error):
    return jsonify(error="Oops! this page does not exist"), 404

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True, ssl_context=(
        './ssl/cert.pem', './ssl/key.pem'))


